#!/bin/bash

finish () {
    exit 0
}

trap finish SIGTERM SIGINT SIGQUIT
# Inifinite sleep
sleep infinity &
wait $!
#/bin/bash
