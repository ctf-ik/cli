
#!/bin/bash

#Changelogs
# JULIEN - 01/12/2021 Mise en place du déploiement des conteneurs wireguard
# OLIVIER - 02/12/2021 Déploiement du cluster K3S
# OLIVIER - 02/12/2021 Ajout des variables dans le script et ajout des étapes
# JULIEN - 03/12/2021 Ajout du menu
# GUILLAUME - 17/02/2022 Clean du code
# JULIEN - 01/03/2022 Modification de l'option -i, ajout des informations des chall pour l'import dans ctfd

#Global variables
DIR_MAIN_ROLE_ANSIBLE=../ansible-scripts
DIR_SCRIPTS=../ansible-scripts/script
FILE_INVENTORIES=../ansible-scripts/inventories/hosts
HOSTS_FILE="../ansible-scripts/inventories/hosts"

# Function that check if ip is in right format
function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

# Default Help message
function usage()
{
    echo "Usage: ./CTFIK.sh :" 1>&2
    echo "Example : ./CTFIK.sh -m 192.168.1.23 -w 192.168.1.24,192.168.1.25 -d"
    echo "                              " 1>&2
    echo "Information about Master and Worker :" 1>&2
    echo "-m    <Master nodes IP address>" 1>&2
    echo "-w    <Worker 1 nodes IP address>,<Worker 2 nodes IP address>,...,<Worker N nodes IP address>" 1>&2
    echo "                              " 1>&2
    echo "Get infrastructure information :" 1>&2
    echo "-i    <challenge>,<wireguard>,<ctfd> challenge container information" 1>&2
    echo "      /!\ You must specifiy -m <Master nodes IP address>" 1>&2
    echo "                              " 1>&2
    echo "Infrastructure deployment :" 1>&2
    echo "-s    csv file with ip,username,password " 1>&2
    echo "      this option gonna setup all remote host with right user pub key and ssh key" 1>&2
    echo "-d    deploy K3S cluster with -m (master) REQUIRED and -w (worker) OPTIONAL information" 1>&2
    echo "-c    challenge_list.csv flag -m REQUIRED, Deploy challenge writed in csv file" 1>&2
    echo "-v    player_list.csv flag -m REQUIRED, Deploy wireguard container and create user configuration in ./user_conf/ ;" 1>&2
    echo "      /!\ WARNING this option will recreate all Wireguard POD so all player will be disconnected" 1>&2
    echo "                              " 1>&2
    echo "Infrastructure remove :" 1>&2
    echo "-r    -m (Master) or -w (Worker) to remove the (non) desired node" 1>&2
    echo "                              " 1>&2
    echo "For more information, please visit our gitlab https://gitlab.com/ctf-ik and contact us."
    exit 1
}

# Local vars
deploy=0
remove=0
worker_ok=0
wireguard=0
create_challenge=0
get_container=0
setup=0
master=0

# Begin logic
while getopts "m:w:v:c:i:s:dhr" option; do
    case "${option}" in
        m)
            m=${OPTARG}
            if  valid_ip $m ; then
                master=1
                master_ip=$m
            else
                echo "CTFIK.sh : -m '$m' Bad address IP format"
                exit
            fi
            ;;
        w)
            w+=("$OPTARG")
            mapfile -t worker_ip_list <<< "${w[@]}"
            array=("${worker_ip_list[@]/,/ }")
            worker_ip_list=($(echo $array | sed 's/\,/\n/g'))
            for ip in "${worker_ip_list[@]}"; do
                if valid_ip $ip ; then
                    worker_ip+="$ip "
                    worker_ok=1
                else
                    echo "CTFIK.sh : -w '$ip' is not in correct format"
                    worker_ok=0
                    exit

                fi
            done
            ;;
        c)
            create_challenge=1
            challenge_list=${OPTARG}
            ;;
        i)
            get_container=1
            container_name=${OPTARG}
            ;;

        v)
            wireguard=1
            user_list=${OPTARG}
            ;;

        s)
            setup=1
            host_list=${OPTARG}
            ;;
        d)
            deploy=1
            ;;

        r)
            remove=1
            ;;

        h | *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

worker_ip=($(echo $worker_ip | tr " " "\n"))

if [[ $worker_ok == 0 && $master == 0 && $setup == 1 ]]; then
     $DIR_SCRIPTS/infra_deploy/setup.sh $host_list
elif [[ -n "${master_ip}" || $worker_ok == 1 && $setup == 0 ]]; then

    echo "[debian_k3s_cluster:children]" > $HOSTS_FILE
    if [ -n "${master_ip}" ]; then
        echo "debian_k3s_master" >> $HOSTS_FILE
    fi
    if [[ "${worker_ok}" == 1 ]]; then
        echo "debian_k3s_workers" >> $HOSTS_FILE
    fi

    echo " " >> $HOSTS_FILE
    if [ -n "${master_ip}" ]; then
        echo "[debian_k3s_master]" >> $HOSTS_FILE
        echo "CTFIk_master1 ansible_host=${master_ip}" >> $HOSTS_FILE
    fi
    if [[ "${worker_ok}" == 1 ]]; then
        echo "     ">> $HOSTS_FILE
        echo "[debian_k3s_workers]">> $HOSTS_FILE
        i=1
        for ip in "${worker_ip[@]}" ; do
            echo "CTFIk_worker$i ansible_host=$ip" >> $HOSTS_FILE
            ((i=i+1))
        done
    fi

    if [[ $remove == 0 && $deploy == 1 && $wireguard == 0 && $create_challenge == 0 && $get_container == 0 ]]; then
        ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_deploy_k3s_cluster.yml -i $FILE_INVENTORIES
        ssh "${master_ip}" 'sudo cp -R /root/.kube /home/ctf-ik/' </dev/null 2>/dev/null
        ssh "${master_ip}" 'sudo chown -R ctf-ik: /home/ctf-ik/' </dev/null 2>/dev/null
	ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_deploy_ctfd_container_k3s.yml -i $FILE_INVENTORIES
        ssh "${master_ip}" 'sudo chown -R ctf-ik: /etc/rancher/k3s/k3s.yaml' </dev/null 2>/dev/null

    elif [[ $remove == 1 && $deploy == 0 && $wireguard == 0 && $create_challenge == 0  && $get_container == 0 ]]; then
        ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_delete_k3s_cluster.yml -i $FILE_INVENTORIES
    elif [[ $remove == 0 && $deploy == 0 && $wireguard == 1 && $create_challenge == 0 && $get_container == 0 ]]; then
        file=$user_list
        if test -f "$file"; then
            $DIR_SCRIPTS/wireguard/wireguard_creation_kubernetes.sh $user_list $master_ip
            ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_deploy_wireguard_container_k3s.yml -i $FILE_INVENTORIES
            #ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_deploy_bind9_container_k3s.yml -i $FILE_INVENTORIES
        else
            echo "CTFIK.sh : -v : Please provide a CSV input with the following format : PLAYERNAME;TEAMNAME;"
            exit 1
        fi
    elif [[ $remove == 0 && $deploy == 0 && $wireguard == 0 && $create_challenge == 1 && $get_container == 0 ]]; then
        file=$challenge_list
        if test -f "$file"; then
            bash  $DIR_SCRIPTS/challenges/deploy_all_challenges.sh $challenge_list
            bash  $DIR_SCRIPTS/challenges/export_to_ctfd.sh $challenge_list
            echo "------ Generation du fichier a importer dans ctfd dans $(pwd)/challenge_for_ctfd.csv ------"

        else
            echo "CTFIK.sh : -c : Please provide a CSV input with the following format : ########METTRE LE FORMAT COMME INDIQUE POUR WIREGUARD##############"
            exit 1
        fi
    elif [[ $remove == 0 && $deploy == 1 && $wireguard == 1 && $create_challenge == 0 && $get_container == 0 ]]; then

        ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_deploy_k3s_cluster.yml -i $FILE_INVENTORIES

        file=$user_list
        if test -f "$file"; then
            $DIR_SCRIPTS/wireguard/wireguard_creation_kubernetes.sh $user_list
            ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_deploy_wireguard_container_k3s.yml -i $FILE_INVENTORIES
        else
            echo "CTFIK.sh : -v : Please provide a CSV input with the following format : PLAYERNAME;TEAMNAME;"
            exit 1
        fi

    elif [[ $remove == 0 && $deploy == 1 && $wireguard == 1 && $create_challenge == 1 && $get_container == 0 ]]; then

        ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_deploy_k3s_cluster.yml -i $FILE_INVENTORIES

        file=$user_list
        if test -f "$file"; then
            $DIR_SCRIPTS/wireguard/wireguard_creation_kubernetes.sh $user_list
            ansible-playbook $DIR_MAIN_ROLE_ANSIBLE/main_deploy_wireguard_container_k3s.yml -i $FILE_INVENTORIES
        else
            echo "CTFIK.sh : -v : Please provide a CSV input with the following format : PLAYERNAME;TEAMNAME"
            exit 1
        fi

        file=$challenge_list
        if test -f "$file"; then
            bash  $DIR_SCRIPTS/challenges/deploy_all_challenges.sh $challenge_list
            bash  $DIR_SCRIPTS/challenges/export_to_ctfd.sh $challenge_list
            echo "------ Generation du fichier a importer dans ctfd dans $(pwd)/challenge_for_ctfd.csv ------"

        else
            echo "CTFIK.sh : -c : Please provide a CSV input with the following format : NOMCHALLENGE,TYPE,PROTOCOLE,PORT,CONTAINERPORT,IMAGE,EMPLACEMENT_CONFIG_FILE,DATA,NOM_FICHIER_CONFIG"
            exit 1
        fi

    elif [[ $remove == 0 && $deploy == 0 && $wireguard == 0 && $create_challenge == 0 && $get_container == 1 ]]; then
        if [[ $container_name == "wireguard" ]]; then
            ssh "${master_ip}" 'kubectl get pods -n wireguard' 2>/dev/null
        elif [[ $container_name == "ctfd" ]]; then
            ssh "${master_ip}" 'kubectl get pods -n ctfd' 2>/dev/null
        elif [[ $container_name == "challenge" ]]; then
            ssh "${master_ip}" 'kubectl get pods -n challenge' 2>/dev/null
        else
            echo "CTFIK.sh : -i please provide input <wireguard>, <ctfd>, <challenge>"
        fi

    elif [[ $remove == 0 && $deploy == 0 && $wireguard == 0 && $create_challenge == 0 && $get_container == 0 ]]; then
        echo "CTFIK.sh : -d -r -v -c -i is required with -m options, -h for more information"
        exit 1
    else
        echo "CTFIK.sh : -d -r  can't be set together, -h for more information"
        exit 1
    fi
else
    usage
fi
